## Sidebar function:

sidebar <- dashboardSidebar(
  sidebarMenu(
    menuItem("Dashboard", tabName = "dashboard", icon = icon("dashboard")),
    menuItem("Widgets", 
             menuSubItem('Index1', tabName = 'widgets1'),
             menuSubItem('Index2', tabName = 'widgets2'),
             menuSubItem('Index3', tabName = 'widgets3'),
             tabName = "widgets", icon = icon("th"))
    
  )
)